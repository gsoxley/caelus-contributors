#include "label.hpp"
#include "wordList.hpp"

namespace CML
{
    class polyMesh;

    label checkTopology(const polyMesh&, const bool, const bool);
}
